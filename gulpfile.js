// PATHS
var path_theme = '';
var path_themeImg = path_theme + 'img';
var path_themeCss = path_theme + 'css';
var path_themeSass = path_theme + 'sass';
var path_themeFonts = path_theme + 'fonts';
var path_themeJs = path_theme + 'js';
var path_templates = path_theme + 'templates';
var path_vendor = 'vendor';
var path_npm = 'node_modules';

// INCLUDES
var gulp = require('gulp');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-cssmin');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var Fs = require('fs');
const del = require('del');

// TASKS ---------------------------------------------------------------------------------------------------------------

//1. CLEAN
gulp.task('clean', function() {
    return del(['output']);
});

//2. TWIG
gulp.task('twig', ['clean'], function () {
    'use strict';
    var twig = require('gulp-twig');
    return gulp.src(path_templates + '/twig/*.twig')
        .pipe(twig({
            data: {
                title: 'Gulp and Twig',
                benefits: [
                    'Fast1',
                    'Flexible1',
                    'Secure1'
                ],
                //data: require('./' + path_templates + '/data/blocks-list.json')
            }
        }))
        .pipe(gulp.dest(path_templates + '/html/'))
});

//3. SASS - Compile Our Sass
gulp.task('sass', ['clean'], function () {
    return gulp.src(path_themeSass + '/*.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        //.pipe(cssmin())
        .pipe(gulp.dest(path_themeCss))
});

//4. JAVASCRIPT -  Concatenate & Minify JS
gulp.task('scripts', ['clean'], function () {
    return gulp.src([
        path_themeJs + '/app/**/*.js'
    ])
        .pipe(concat('app.js'))
        .pipe(gulp.dest(path_themeJs))
        .pipe(rename('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(path_themeJs));
});

//5. SVG FONTS - Create icon fonts from several SVG icons
gulp.task('iconfont', ['clean'], function () {
    return gulp.src([path_themeImg + '/svg/*.svg'])
        .pipe(iconfontCss({
            fontName: 'svg-font',
            path: path_themeSass + '/svg-fonts/_svg-font.templ',
            targetPath: '../../sass/svg-fonts/_svg-font.scss',
            fontPath: '../fonts/svg/'
        }))
        .pipe(iconfont({
            fontName: 'svg-font',
            formats: ['ttf', 'eot', 'woff', 'woff2'],
            normalize: true,
            centerHorizontally: false,
            fixedWidth: false,
            fontHeight: 1000
        }))
        .on('glyphs', function(glyphs, options) {
            //console.log(glyphs, options);
            var jsonData = {
                fontName: options.fontName,
                fontFormats: options.formats,
                glyphs: glyphs.map(function (glyph, options) {
                    // This line is needed because gulp-iconfont has changed the api from 2.0
                    return {
                        name: glyph.name,
                        codepoint: glyph.unicode[0].charCodeAt(0).toString(16).toUpperCase()
                    }
                })
            };
            Fs.writeFile(path_templates + "/data/svg-font.json", JSON.stringify(jsonData, null, 2), function (error) {
                //if (error) return console.log($taskId, error);
                //$plugins.Util.log($taskId, 'JSON created', $taskOptions.jsonPath);
            });
        })
        .pipe(gulp.dest(path_themeFonts + '/svg'));
});

//----------------------------------------------------------------------------------------------------------------------

// Watch Files For Changes
gulp.task('watch', function () {
    gulp.watch([path_themeJs +   '/**/*.js', '!' + path_themeJs + '/**/app?(.min).js'], ['scripts']);
    gulp.watch(path_themeSass + '/**/*.scss', ['sass']);
    gulp.watch(path_templates + '/twig/**/*.twig', ['twig']);
    gulp.watch(path_templates + '/data/**/*.json', ['twig']);
});

// Default Task
gulp.task('default', [
    'twig'
    ,'sass'
    ,'scripts'
    ,'iconfont'
    ,'watch'
]);
