// Project examples script - remove this is unnecessary
examples =
{
    init: function () {
        examples.examplesAjaxTest1('.js-ajax-btn1');
        examples.examplesAjaxTest2('.js-ajax-btn2');
        examples.examplesAjaxTest3('.js-ajax-btn3');
    },
    examplesAjaxTest3: function (button) {
        $(button).on('click', function () {
            $.ajax({
                method: "POST",
                url: "/examples/ajax/test3/some.php",
                data: { name: "John", location: "Boston" }
            })
                .done(function( msg ) {
                    alert( "Data Saved: " + msg );
                });
        });
    },
    examplesAjaxTest2: function (button) {
        $(button).on('click', function () {
            $.ajax({
                method: 'GET',
                url: '/examples/ajax/test2/test.js',
                dataType: 'script'
            });
        });
    },
    examplesAjaxTest1: function (button) {
        $(button).on('click', function () {
            $.ajax({
                url: '/examples/ajax/test1/test.html',
                cache: false
            })
                .done(function( html ) {
                    $( '.js-ajax1' ).append( html );
                    $(button).remove();
                });
        });
    }
};

//Theme main options
examples.options = {};

//Theme main containers as jquery objects
examples.containers = {};

$(examples.init);
