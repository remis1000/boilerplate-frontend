// Project custom script

app =
{
    init: function () {
        app.test(app.containers.submitBtn);
    },
    test: function (button) {
        $(button).on("click", function () {
            alert('button pressed :)');
        });
    }
};

//Theme main options
app.options = {};

//Theme main containers as jquery objects
app.containers = {
    submitBtn: $('#submitBtn')
};

$(app.init);