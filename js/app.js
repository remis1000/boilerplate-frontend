// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.
// Project examples script - remove this is unnecessary
examples =
{
    init: function () {
        examples.examplesAjaxTest1('.js-ajax-btn1');
        examples.examplesAjaxTest2('.js-ajax-btn2');
        examples.examplesAjaxTest3('.js-ajax-btn3');
    },
    examplesAjaxTest3: function (button) {
        $(button).on('click', function () {
            $.ajax({
                method: "POST",
                url: "/examples/ajax/test3/some.php",
                data: { name: "John", location: "Boston" }
            })
                .done(function( msg ) {
                    alert( "Data Saved: " + msg );
                });
        });
    },
    examplesAjaxTest2: function (button) {
        $(button).on('click', function () {
            $.ajax({
                method: 'GET',
                url: '/examples/ajax/test2/test.js',
                dataType: 'script'
            });
        });
    },
    examplesAjaxTest1: function (button) {
        $(button).on('click', function () {
            $.ajax({
                url: '/examples/ajax/test1/test.html',
                cache: false
            })
                .done(function( html ) {
                    $( '.js-ajax1' ).append( html );
                    $(button).remove();
                });
        });
    }
};

//Theme main options
examples.options = {};

//Theme main containers as jquery objects
examples.containers = {};

$(examples.init);

// Project custom script

app =
{
    init: function () {
        app.test(app.containers.submitBtn);
    },
    test: function (button) {
        $(button).on("click", function () {
            alert('button pressed :)');
        });
    }
};

//Theme main options
app.options = {};

//Theme main containers as jquery objects
app.containers = {
    submitBtn: $('#submitBtn')
};

$(app.init);