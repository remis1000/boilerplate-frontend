/**
 * Created by remis on 15.10.21.
 */
//Not use this file, copy some functions to main.js
app_special =
{
    init: function () {
        app_special.onlySTUFF();
        app_special.toggleClass("#btn", "#elementToggle", ".active");
        app_special.initGoTop("#goTopBtn");//tested
        app_special.scrollingAnimations(true);//tested
        app_special.initFullHeight("#element", "#minusSizeElement", true);
        app_special.initFontSizeResize(".jsFontSizeResize");
        app_special.scrollingDownOnClick("#scrollDownBtn", "#scrollTarget");//tested
    },
    onlySTUFF: function() {

        //Suff nr1
        $(document).mouseup(function (e)
        {
            var container = $('element');
            if (!container.is(e.target)
                && container.has(e.target).length === 0)
            {
                //action on click out of element
            }
        });

        //Suff nr2
        $('element').hover(function(){
            //action on hover
        }, function(){
            //action on unhover
        });

    },
    scrollingDownOnClick: function (btn, target) {
        $(btn).on("click", function () {
            app_special.containers.bodyHtml.animate({
                scrollTop: app_special.containers.body.find(target).offset().top
            }, 800);
        });
    },

    initFontSizeResize: function (element) {
        $(window).on('DOMContentLoaded load resize scroll', function () {
            var fontSize = 100;
            var screen_lg = 1100;
            var screen_sm = 768;
            if (viewport().width >= screen_sm && viewport().width < screen_lg) {
                fontSize = 100 * parseInt(viewport().width) / screen_lg;
                //console.log("mobile");
            } else {
                fontSize = 100;
                //console.log("pc");
            }
            $(element).css("font-size", fontSize + "%");
        });
    },

    initFullHeight: function (element, minusSize, bindEvent) {

        var windowHeight = viewport().height;
        var minusSizeElement = parseInt($(minusSize).outerHeight());//some home navbar..
        $(element).height(windowHeight - minusSizeElement);

        if (bindEvent)
            app_special.containers.body.on("resize", function () {
                app_special.initFullHeight(element, minusSize, false);
            });
    },

    scrollingAnimations: function (bindEvent) {

        var top_correctio = 50;

        $(function ($, win) {
            $.fn.inViewport = function (cb) {
                return this.each(function (i, el) {
                    function visPx() {
                        var H = $(this).height(),
                            r = el.getBoundingClientRect(), t = r.top + top_correctio, b = r.bottom;
                        return cb.call(el, Math.max(0, t > 0 ? H - t : (b < H ? b : H)));
                    }

                    visPx();
                    $(win).on("resize scroll", visPx);
                });
            };
        }(jQuery, window));

        if (viewport().width > 767) {
            $(".jsAnimationTarget").inViewport(function (px) {
                if (px) {
                    $(this).addClass("animation");
                } else {
                    $(this).removeClass("animation");
                }
            });
        }

        if (bindEvent)
            app_special.containers.body.on("resize", function () {
                app_special.scrollingAnimations(false);
            });

    },
    initGoTop: function (btn) {
        $(btn).on("click", function(){
            app_special.containers.bodyHtml.animate({
                scrollTop: $("html").offset().top
            }, 1000);
        });
    },
    toggleClass: function (button, element, className) {
        $(button).on("click", function () {
            $(element).toggleClass(className);
        });
    }
};

app_special.containers = {
    body: $("body"),
    bodyHtml: $("html, body")
};

$(app_special.init);

//Vievport
function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {width: e[a + 'Width'], height: e[a + 'Height']};
}

//Vievport
function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {width: e[a + 'Width'], height: e[a + 'Height']};
}

//jQuery resize
(function ($, h, c) {
    var a = $([]), e = $.resize = $.extend($.resize, {}), i, k = "setTimeout", j = "resize", d = j + "-special-event", b = "delay", f = "throttleWindow";
    e[b] = 250;
    e[f] = true;
    $.event.special[j] = {
        setup: function () {
            if (!e[f] && this[k]) {
                return false
            }
            var l = $(this);
            a = a.add(l);
            $.data(this, d, {w: l.width(), h: l.height()});
            if (a.length === 1) {
                g()
            }
        }, teardown: function () {
            if (!e[f] && this[k]) {
                return false
            }
            var l = $(this);
            a = a.not(l);
            l.removeData(d);
            if (!a.length) {
                clearTimeout(i)
            }
        }, add: function (l) {
            if (!e[f] && this[k]) {
                return false
            }
            var n;

            function m(s, o, p) {
                var q = $(this), r = $.data(this, d);
                r.w = o !== c ? o : q.width();
                r.h = p !== c ? p : q.height();
                n.apply(this, arguments)
            }

            if ($.isFunction(l)) {
                n = l;
                return m
            } else {
                n = l.handler;
                l.handler = m
            }
        }
    };
    function g() {
        i = h[k](function () {
            a.each(function () {
                var n = $(this), m = n.width(), l = n.height(), o = $.data(this, d);
                if (m !== o.w || l !== o.h) {
                    n.trigger(j, [o.w = m, o.h = l])
                }
            });
            g()
        }, e[b])
    }
})(jQuery, this);