Naming conventions:

CLASSES
.module
.module-child
.module--modifier

CLASSES helpers, layout, js, state
.h-helper
.l-layout-helper
.js-classDedicatedOnlyJS
.is-state

ID
#element-id


Examples:

.slider{}
.slider--partners{}
.slider-title{}
.slider-image{}
.slider-image--circle{}

.h-mt10{}
.h-text-truncate{}
.l-flex{}
.js-toggle
.is-active

# slider-home{}


* Modulio pavadinimui galima taikyti preffix, pvz: ub-module, ub-module-child, ub-module--modifier
   prefix yra labai gerai, kai norima isskirti savo rasytus modulius nuo kitu (wordpress, presta...) ar framework klasiu (bootstrap, foundation...)
